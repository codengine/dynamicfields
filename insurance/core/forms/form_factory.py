from django import forms
from django.db import transaction
from core.forms.base_form import BaseForm
from insurance.models.risk_field_value import RiskFieldValue
from insurance.models.risk_type import RiskType
from insurance.models.risk_type_row_counter import RiskTypeRowCounter


class FormFactory(object):

    """
    form_name is the new form to be created.
    fields are list of tuples. Like [("name", forms.CharField(max_length=255, default=""))]
    BaseForm is the super class.
    """
    @classmethod
    def build_form(cls, form_name, BaseForm=BaseForm):
        # initial can be passed as initial={field_name: value} format

        def __init__(self, **kwargs):
            initial = kwargs.get("initial", {})
            fields = kwargs.get("fields", {})
            risk_type_id = kwargs.get("risk_type_id")

            self.fields = {
                "risk_type_id": forms.IntegerField(initial=risk_type_id,
                                                   widget=forms.HiddenInput(attrs={"class": "form-control"}))
            }

            initial["risk_type_id"] = risk_type_id
            self.data = initial
            self.cleaned_data = {}
            for field_name, form_field in fields.items():
                form_field.widget.attrs["class"] = "form-control"
                self.fields[field_name] = form_field
                if initial:
                    self.fields[field_name].initial = initial.get(field_name, None)

        def is_valid(self):
            for field_name, field in self.fields.items():
                if field_name == "risk_type_id":
                    self.cleaned_data["risk_type_id"] = field.clean(self.data[field_name])
                else:
                    if field.required:
                        if not self.data.get(field_name):
                            return False
                    try:
                        cleaned_value = field.clean(self.data[field_name])
                        self.cleaned_data[field_name] = cleaned_value
                    except Exception as exp:
                        return False
            return True

        def save(self, **kargs):
            with transaction.atomic():
                risk_type_id = self.cleaned_data["risk_type_id"]
                risk_type_objects = RiskType.objects.filter(pk=risk_type_id)
                if risk_type_objects.exists():
                    risk_type_object = risk_type_objects.first()
                    row_id = self.cleaned_data.get("row_id", None)
                    if row_id:
                        # It's an edit
                        risk_field_objects = risk_type_object.fields.all()
                        for risk_field in risk_field_objects:
                            cleaned_value = self.cleaned_data.get(risk_field.field_name, None)
                            risk_field_value_objects = RiskFieldValue.objects.filter(risk_type_id=risk_type_object.pk, row_id=row_id, field_id=risk_field.pk)
                            if risk_field_value_objects.exists():
                                risk_field_value_object = risk_field_value_objects.first()
                                risk_field_value_object.value = cleaned_value
                                risk_field_value_object.save()
                    else:
                        # It's a new instance of risk type
                        risk_type_row_counter_objects = RiskTypeRowCounter.objects.filter(risk_type_id=risk_type_id)
                        if risk_type_row_counter_objects.exists():
                            risk_type_row_counter_object = risk_type_row_counter_objects.first()
                            row_id = risk_type_row_counter_object.value + 1
                            risk_type_row_counter_object.value = row_id
                            risk_type_row_counter_object.save()
                        else:
                            row_id = 1
                            risk_type_row_counter_object = RiskTypeRowCounter(risk_type_id=risk_type_id, value=row_id)
                            risk_type_row_counter_object.save()

                        risk_field_objects = risk_type_object.fields.all()
                        for risk_field in risk_field_objects:
                            cleaned_value = self.cleaned_data.get(risk_field.field_name, None)
                            risk_field_value_object = RiskFieldValue(risk_type_id=risk_type_object.pk, row_id=row_id)
                            risk_field_value_object.field_id = risk_field.pk
                            risk_field_value_object.value = cleaned_value
                            risk_field_value_object.save()

                    return row_id

        NewForm = type(str(form_name), (BaseForm,), {"__init__": __init__, "is_valid": is_valid, "save": save})
        return NewForm