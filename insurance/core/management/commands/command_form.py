from django.core.management.base import BaseCommand

from core.forms.form_factory import FormFactory
from insurance.models.risk_type import RiskType


class Command(BaseCommand):

    def create_automobile_risk_instance(self, risk_name, risk_type_id, fields={}, initial={}):
        # data = {
        #     'name': "Car",
        #     "is_active": True,
        #     "service_years": 10
        # }

        AutoMobileForm = FormFactory.build_form(risk_name)

        automobile_form_instance = AutoMobileForm(risk_type_id=risk_type_id, fields=fields, initial=initial)

        if automobile_form_instance.is_valid():
            automobile_form_instance.save()

    def handle(self, *args, **options):
        auto_mobile_instance = RiskType.objects.filter(name="Automobile").first()
        # fields = auto_mobile_instance.fields.all()
        form_name = "Automobile"
        risk_type_id = auto_mobile_instance.pk

        risk_fields = (
        'id', 'field_name', "field_description", "field_type", "default_value", "is_required", "field_choices")

        fields = {}

        risk_field_objects = auto_mobile_instance.fields.filter(field_name__in=risk_fields)

        for risk_field_object in auto_mobile_instance.fields.all():
            fields[risk_field_object.field_name] = risk_field_object.get_form_field()

        data = {
            'name': "Car",
            "is_active": True,
            "service_years": 10
        }

        AutoMobileForm = FormFactory.build_form(form_name)

        # automobile_form_instance = AutoMobileForm(risk_type_id=risk_type_id, fields=fields, initial=data)
        #
        # if automobile_form_instance.is_valid():
        #     automobile_form_instance.save()
        #
        # self.create_automobile_risk_instance(form_name, risk_type_id, fields, data)
        # data = {
        #     'name': "Test1",
        #     "is_active": True,
        #     "service_years": 20
        # }
        # self.create_automobile_risk_instance(form_name, risk_type_id, fields, data)
        # data = {
        #     'name': "Test2",
        #     "is_active": False,
        #     "service_years": 15
        # }
        # self.create_automobile_risk_instance(form_name, risk_type_id, fields, data)

        queryset = RiskType.objects.get_pivoted_data(risk_type_id, search_keyword='C')
        print(queryset)

