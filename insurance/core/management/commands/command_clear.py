from django.core.management.base import BaseCommand
from insurance.models.risk_field import RiskField
from insurance.models.risk_field_value import RiskFieldValue
from insurance.models.risk_type import RiskType
from insurance.models.risk_type_row_counter import RiskTypeRowCounter


class Command(BaseCommand):

    def handle(self, *args, **options):
        RiskField.objects.all().delete()
        RiskType.objects.all().delete()
        RiskFieldValue.objects.all().delete()
        RiskTypeRowCounter.objects.all().delete()