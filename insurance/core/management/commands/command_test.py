from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.db.models.fields import IntegerField

from core.forms.form_factory import FormFactory
from insurance.models.risk_field import RiskField
from insurance.models.risk_field_value import RiskFieldValue
from insurance.models.risk_type import RiskType
from insurance.models.risk_type_row_counter import RiskTypeRowCounter


class Command(BaseCommand):

    def handle(self, *args, **options):
        # from django import forms
        #
        # data = {
        #     "name": forms.CharField(max_length=255)
        # }
        #
        # Form = FormFactory.build_form(form_name="Form", risk_type_id=1, fields=data)
        # f = Form()
        # p = f.is_valid()
        # print(p)
        #
        # r = RiskType.objects.get_pivot_data(1)
        # print(r)

        # Clear all data
        a = RiskType(name="Automobile")
        c = RiskType(name="Car")
        l = RiskType(name="Life")
        # self.risks = [lambda x: x.save() for x in [a, c, l]]
        a.save()
        c.save()
        l.save()

        # Risk Fields
        automobile_fields = [
            {
                "field_name": "name",
                "field_description": "",
                "field_type": "text",
                "default_value": "",
                "is_required": True
            },
            {
                "field_name": "is_active",
                "field_description": "",
                "field_type": "bool",
                "default_value": True,
                "is_required": False
            },
            {
                "field_name": "service_years",
                "field_description": "",
                "field_type": "int",
                "default_value": 0,
                "is_required": False
            }
        ]

        a1, a2, a3 = RiskField(**automobile_fields[0]), RiskField(**automobile_fields[1]), RiskField(**automobile_fields[2])
        a1.save()
        a2.save()
        a3.save()
        a.fields.add(a1, a2, a3)

        car_fields = [
            {
                "field_name": "name",
                "field_description": "",
                "field_type": "text",
                "default_value": "",
                "is_required": True
            },
            {
                "field_name": "is_active",
                "field_description": "",
                "field_type": "bool",
                "default_value": True,
                "is_required": False
            }
        ]

        c1, c2 = RiskField(**car_fields[0]), RiskField(**car_fields[1])
        c1.save()
        c2.save()
        c.fields.add(c1, c2)

        life_fields = [
            {
                "field_name": "name",
                "field_description": "",
                "field_type": "text",
                "default_value": "",
                "is_required": True
            },
            {
                "field_name": "details",
                "field_description": "",
                "field_type": "long_text",
                "default_value": "",
                "is_required": False
            },
            {
                "field_name": "is_active",
                "field_description": "",
                "field_type": "bool",
                "default_value": True,
                "is_required": False
            },
            {
                "field_name": "service_years",
                "field_description": "",
                "field_type": "int",
                "default_value": 0,
                "is_required": False
            }
        ]
        l1, l2, l3 = RiskField(**life_fields[0]), RiskField(**life_fields[1]), RiskField(
            **life_fields[2])
        l1.save()
        l2.save()
        l3.save()
        l.fields.add(l1, l2, l3)