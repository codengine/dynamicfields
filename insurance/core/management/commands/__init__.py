from django.core.management.base import BaseCommand, CommandError
from django import forms

from core.forms.form_factory import FormFactory


class Command(BaseCommand):
    def handle(self, *args, **options):
        data = {
            "name": forms.CharField(max_length=255)
        }

        Form = FormFactory.build_form(form_name="Name", fields=data)
        f = Form()
        f.is_valid()
