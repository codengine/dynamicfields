from django.db import models
from core.models.base_entity import BaseEntity
from insurance.models.risk_type import RiskType


class RiskTypeRowCounter(BaseEntity):
    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE)
    value = models.IntegerField(default=0)

    def reset_count(self, value=0):
        self.value = value
        self.save()