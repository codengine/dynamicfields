from django.db import models
from django.db.models.query_utils import Q

from core.models.base_entity import BaseEntity
from insurance.models.managers.risk_type_model_manager import RiskTypeModelManager
from insurance.models.risk_field import RiskField


class RiskType(BaseEntity):
    name = models.CharField(max_length=200)
    fields = models.ManyToManyField(RiskField)

    objects = RiskTypeModelManager()

    def get_fields(self):
        return self.fields.all()

    def __str__(self):
        return self.name