from django.db import models
from core.models.base_entity import BaseEntity
from insurance.models.risk_field import RiskField
from insurance.models.risk_type import RiskType


class RiskFieldValue(BaseEntity):
    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE)
    row_id = models.IntegerField(default=0)
    field = models.ForeignKey(RiskField, on_delete=models.CASCADE)
    value = models.CharField(max_length=5000, blank=True, null=True)

    def save(self, *args, **kwargs):
        super(RiskFieldValue, self).save(*args, **kwargs)

    @property
    def cleaned_value(self):
        form_field = self.get_form_field()
        cleaned_value = form_field.clean(self.value)
        return cleaned_value

    def clean(self):
        form_field = self.get_form_field()
        form_field.clean(self.value)
        return super(RiskFieldValue, self).clean()

    def get_form_field(self):
        return self.field.get_form_field()