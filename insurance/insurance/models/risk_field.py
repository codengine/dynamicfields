from django import forms
from django.db import models
from django.apps import apps
from core.models.base_entity import BaseEntity
from rest_framework import serializers

field_choices = (
            ('text', 'Text'),
            ('long_text', 'Long Text'),
            ('int', 'Integer'),
            ('float', 'Floating point/Decimal'),
            ('bool', 'Boolean True/False'),
            ('enum', 'Choices'),
            ('date', 'Date'),
            ('datetime', 'Date Time'),
        )


class RiskField(BaseEntity):
    field_name = models.CharField(max_length=200)
    field_description = models.TextField(null=True, blank=True)
    field_type = models.CharField(max_length=10, choices=field_choices, default='text')
    default_value = models.CharField(max_length=5000, blank=True, help_text="You may leave blank. Use True/False for Boolean")
    is_required = models.BooleanField(default=False)
    field_choices = models.CharField(max_length=2000, blank=True, help_text="Custom choices")
    placeholder = models.CharField(max_length=2000, blank=True, help_text="Placeholder")

    def __str__(self):
        return self.field_name + "( %s )" % self.field_type

    def get_form_field(self):
        universal_kwargs = {
            'initial': self.default_value if self.default_value else '',
            'required': self.is_required if self.is_required else False,
            # 'placeholder': self.placeholder if self.placeholder else ''
        }
        if self.field_type == "bool":
            return forms.BooleanField(**universal_kwargs)
        elif self.field_type == "int":
            return forms.IntegerField(**universal_kwargs)
        elif self.field_type == "float":
            return forms.FloatField(**universal_kwargs)
        elif self.field_type == "text":
            return forms.CharField(widget=forms.TextInput, max_length=255, **universal_kwargs)
        elif self.field_type == "long_text":
            return forms.CharField(widget=forms.Textarea, max_length=5000, **universal_kwargs)
        elif self.field_type == "enum":
            choices = self.field_choices.split(',')
            if self.is_required is True:
                select_choices = ()
            else:
                select_choices = (('', '---------'),)
            for choice in choices:
                select_choices = select_choices + ((choice, choice),)
            return forms.ChoiceField(
                choices=select_choices, **universal_kwargs)
        elif self.field_type == "date":
            return forms.DateField(**universal_kwargs)
        elif self.field_type == "datetime":
            return forms.DateTimeField(**universal_kwargs)
        return forms.CharField(**universal_kwargs)

    def get_serializer_field(self):
        universal_kwargs = {
            'initial': self.default_value if self.default_value else '',
            'required': self.is_required if self.is_required else False,
        }
        if self.field_type == "bool":
            return serializers.BooleanField(**universal_kwargs)
        elif self.field_type == "int":
            return serializers.IntegerField(**universal_kwargs)
        elif self.field_type == "float":
            return serializers.FloatField(**universal_kwargs)
        elif self.field_type == "text":
            return serializers.CharField(max_length=255, **universal_kwargs)
        elif self.field_type == "long_text":
            return serializers.CharField(max_length=5000, **universal_kwargs)
        elif self.field_type == "enum":
            choices = self.field_choices.split(',')
            if self.is_required is True:
                select_choices = ()
            else:
                select_choices = (('', '---------'),)
            for choice in choices:
                select_choices = select_choices + ((choice, choice),)
            return serializers.ChoiceField(
                choices=select_choices, **universal_kwargs)
        elif self.field_type == "date":
            return serializers.DateField(**universal_kwargs)
        elif self.field_type == "datetime":
            return serializers.DateTimeField(**universal_kwargs)
        return serializers.CharField(**universal_kwargs)

    # @classmethod
    # def get_all_free_fields(cls):
    #     return RiskField.objects.filter(risktype_set__isnull=False).values_list('pk', flat=True)

    @staticmethod
    def get_all_free_fields(include_fields=[]):
        RiskType = apps.get_model('insurance', 'RiskType')
        risk_type_objects = RiskType.objects.all().prefetch_related('fields')
        all_fields_used = []
        for rt in risk_type_objects:
            all_fields_used += rt.fields.values_list('pk', flat=True)
        all_fields_used = list(set(all_fields_used) - set(include_fields))
        return RiskField.objects.exclude(pk__in=all_fields_used)

    def __str__(self):
        return self.field_name + "(%s)" % self.field_type