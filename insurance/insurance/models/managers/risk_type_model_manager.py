from django.db import models
from django.db.models.aggregates import Count, Max
from django.db.models.fields import IntegerField, FloatField, DateField, DateTimeField, BooleanField, \
    TextField
import operator
from django.apps import apps
from django.db.models import CharField, Case, When
from django.db.models.query_utils import Q


class RiskTypeModelManager(models.Manager):

    def get_queryset(self):
        return super(RiskTypeModelManager, self).get_queryset()

    def get_pivoted_data(self, risk_type_id, risk_id=None, search_keyword=None, search_filters={}, filter_columns = []):
        RiskType = apps.get_model('insurance', 'RiskType')
        RiskFieldValue = apps.get_model('insurance', 'RiskFieldValue')
        risk_type_instance = RiskType.objects.get(pk=risk_type_id)
        risk_fields = risk_type_instance.fields.values_list("id", "field_name", "field_type")
        
        output_field_widget_mapper = {
            "int": IntegerField(),
            "text": CharField(),
            "bool": BooleanField(),
            "float": FloatField(),
            "long_text": TextField(),
            "enum": CharField(),
            "date": DateField(),
            "datetime": DateTimeField()
        }

        annotate_params = {}
        column_names = ["row_id"]

        field_mapper = {}
        for risk_field in risk_fields:
            field_mapper[risk_field[0]] = {"field_name": risk_field[1], "type": risk_field[2]}
            column_names += [risk_field[1]]

        _filtered_columns = []
        for column in filter_columns:
            if column in column_names:
                _filtered_columns += [column]

        if not _filtered_columns:
            _filtered_columns = column_names

        _filters = Q()

        if search_keyword:
            for column_name in column_names:
                if _filters:
                    _filters = operator.or_(_filters, Q(**{"%s__icontains" % column_name: search_keyword}))
                else:
                    _filters = operator.or_(_filters, Q(**{"%s__icontains" % column_name: search_keyword}))

        elif search_filters:
            for key, value in search_filters.items():
                if key in _filtered_columns:
                    if _filters:
                        _filters = operator.or_(_filters, Q(**{"%s__icontains" % key: value}))
                    else:
                        _filters = operator.or_(_filters, Q(**{"%s__icontains" % key: value}))
            
        for field_id, field_options in field_mapper.items():
            field_name = field_options["field_name"]
            field_type = field_options["type"]
            output_field_widget = output_field_widget_mapper.get(field_type, None)
            annotate_params[field_name] = Max(Case(When(field_id=field_id, then="value"), output_field=output_field_widget))
        risk_field_value_objects = RiskFieldValue.objects.filter(risk_type_id=risk_type_id).values('row_id').annotate(**annotate_params).values(*column_names)
        if _filters:
            risk_field_value_objects = risk_field_value_objects.filter(_filters)
        # risk_field_value_objects = risk_field_value_objects.values(*_filtered_columns)
        if risk_id:
            if risk_field_value_objects:
                return risk_field_value_objects.filter(row_id=risk_id)
        return risk_field_value_objects
