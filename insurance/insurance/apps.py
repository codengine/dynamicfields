from django.apps import AppConfig


class CustomfieldConfig(AppConfig):
    name = 'customfield'
