from django.conf.urls import url

from insurance.api.viewsets.risk_api_view import RiskAPIView
from insurance.api.viewsets.risk_field_model_viewset import RiskFieldModelViewset
from insurance.api.viewsets.risk_type_model_viewset import RiskTypeModelViewset
from insurance.api.viewsets.risk_viewsets import RiskListAPIView
from insurance.api.viewsets.url_list_api_view import ApiUrlListView

urlpatterns = [
    url(r'^urls/$', ApiUrlListView.as_view(), name='url_list'),
    url(r'^risk-types/$', RiskTypeModelViewset.as_view({'get': 'list'}), name='risk_types'),
    url(r'^risk-fields/(?P<risk_type_id>\d+)/$', RiskFieldModelViewset.as_view({'get': 'list'}), name='risk_fields'),
    url(r'^risk-list/(?P<risk_type_id>\d+)/$', RiskListAPIView.as_view(), name='risk_list'),
    url(r'^risk-create/(?P<risk_type_id>\d+)/$', RiskAPIView.as_view(), name='risk_create'),
]