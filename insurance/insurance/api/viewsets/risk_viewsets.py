from rest_framework.pagination import PageNumberPagination
from core.api.viewsets.base_api_view import BaseAPIView
from insurance.models.risk_type import RiskType


class RiskListAPIView(BaseAPIView, PageNumberPagination):
    http_method_names = ["get"]

    def collect_filters(self, request):
        keyword = request.GET.get('q', None)
        return keyword

    def get_queryset(self, request):
        risk_type_id = self.kwargs.get("risk_type_id")
        risk_type_id = int(risk_type_id)
        filter_keyword = self.collect_filters(request)
        queryset = RiskType.objects.get_pivoted_data(risk_type_id=risk_type_id, search_keyword=filter_keyword)
        return self.paginate_queryset(queryset, request)