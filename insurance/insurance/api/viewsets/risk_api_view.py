from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.response import Response

from core.api.viewsets.base_api_view import BaseAPIView
from core.forms.form_factory import FormFactory
from insurance.models.risk_type import RiskType


class RiskAPIView(BaseAPIView):
    # http_method_names = ["post", "put"]

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        risk_type_id = kwargs.get("risk_type_id")
        risk_type_id = int(risk_type_id)
        risk_type_objects = RiskType.objects.filter(pk=risk_type_id)
        if risk_type_objects.exists():
            risk_type_object = risk_type_objects.first()

            fields = {}

            for risk_field_object in risk_type_object.fields.all():
                fields[risk_field_object.field_name] = risk_field_object.get_form_field()

            post_data = {}
            for key, item in request.data.items():
                post_data[key] = item

            RiskTypeForm = FormFactory.build_form(form_name=risk_type_object.name.capitalize())

            risk_form_instance = RiskTypeForm(risk_type_id=risk_type_id, fields=fields, initial=post_data)

            if risk_form_instance.is_valid():
                risk_row_id = risk_form_instance.save()

                risk_objects = RiskType.objects.get_pivoted_data(risk_type_id=risk_type_id, risk_id=risk_row_id)
                if risk_objects.exists():
                    risk_object = risk_objects[0]
                else:
                    risk_object = {}
                return Response(risk_object, status=status.HTTP_201_CREATED)
        return Response({"message": "Invalid Data"}, status=status.HTTP_400_BAD_REQUEST)