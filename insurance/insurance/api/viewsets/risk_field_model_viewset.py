from core.api.viewsets.base_model_viewset import BaseModelViewSet
from insurance.api.serializers.risk_field_serializer import RiskFieldSerializer
from insurance.models.risk_field import RiskField
from insurance.models.risk_type import RiskType


class RiskFieldModelViewset(BaseModelViewSet):
    serializer_class = RiskFieldSerializer
    queryset = RiskField.objects.all()

    def get_queryset(self):
        risk_type_id = self.kwargs.get("risk_type_id")
        risk_type_id = int(risk_type_id)
        risk_type_objects = RiskType.objects.filter(pk=risk_type_id)
        if risk_type_objects.exists():
            risk_type_object = risk_type_objects.first()
            return risk_type_object.fields.all()
        return RiskField.objects.none()