from core.api.viewsets.base_model_viewset import BaseModelViewSet
from insurance.api.serializers.risk_type_serializer import RiskTypeModelSerializer
from insurance.models.risk_type import RiskType


class RiskTypeModelViewset(BaseModelViewSet):
    serializer_class = RiskTypeModelSerializer
    queryset = RiskType.objects.all()