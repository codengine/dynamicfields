from core.api.serializers.base_model_serializer import BaseModelSerializer
from insurance.models.risk_field import RiskField


class RiskFieldSerializer(BaseModelSerializer):

    class Meta:
        model = RiskField
        fields = ('id', 'field_name', "field_description", "field_type", "default_value", "is_required", "field_choices")