from core.api.serializers.base_model_serializer import BaseModelSerializer
from insurance.api.serializers.risk_field_serializer import RiskFieldSerializer
from insurance.models.risk_type import RiskType


class RiskTypeModelSerializer(BaseModelSerializer):
    fields = RiskFieldSerializer(many=True)

    class Meta:
        model = RiskType
        fields = ('id', 'name', 'fields')