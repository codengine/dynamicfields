from django.contrib import admin
# Register your models here.
from django.contrib.admin.options import ModelAdmin

from insurance.models.risk_field import RiskField
from insurance.models.risk_field_value import RiskFieldValue
from insurance.models.risk_type import RiskType
from insurance.models.risk_type_row_counter import RiskTypeRowCounter


class RiskTypeModelAdmin(ModelAdmin):
    fields = ('name', 'fields')

    def get_form(self, request, obj=None, **kwargs):
        form = super(RiskTypeModelAdmin, self).get_form(request, obj, **kwargs)
        include_fields = obj.fields.values_list('pk', flat=True) if obj else []
        form.base_fields['fields'].queryset = RiskField.get_all_free_fields(include_fields=include_fields)
        return form


class RiskFieldModelAdmin(ModelAdmin):
    fields = ('field_name', 'field_description', 'field_type', 'default_value', 'is_required', 'field_choices', 'placeholder')


admin.site.register(RiskType, RiskTypeModelAdmin)
admin.site.register(RiskField, RiskFieldModelAdmin)
# admin.site.register(RiskFieldValue)
# admin.site.register(RiskTypeRowCounter)