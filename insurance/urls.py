"""insurance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include

import insurance
from insurance.api.viewsets.risk_field_model_viewset import RiskFieldModelViewset
from insurance.api.viewsets.risk_type_model_viewset import RiskTypeModelViewset
from insurance.views.home_views import HomeView
from rest_framework import routers

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^test-url$', HomeView.as_view(), name='home'),
    url(r'^api/', include("insurance.api.urls"))
    # API
    # url(r'^$', HomeView.as_view(), name='home'),
]

# router = routers.SimpleRouter()
# router.register(r'risk-types', RiskTypeModelViewset)
# router.register(r'risk-fields', RiskFieldModelViewset)
# urlpatterns += router.urls

urlpatterns += [

]

