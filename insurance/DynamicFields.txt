RiskType
=========================
1. id
2. name
3. details
4. fields m2m to RiskField table. RiskTypeFields table below takes care of that.

RiskField
==========================
1. id
2. field_name
3. field_type

RiskTypeFields
==========================
1. risk_type_id
2. risk_field_id

RiskFieldValue
==========================
1. id
2. row_id
3. risk_field_id FKey to RiskField
4. value

RiskTypeRowCount
==========================
1. id
2. risk_type_id FKey to RiskType
3. row_number

RiskTypeEntries
==========================
This is a view which will create a pivot table combining the RiskField and RiskFieldValue table.


